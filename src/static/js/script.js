
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
window.onload = function() {
    document.getElementsByTagName("H1")[0].setAttribute("class", "vamp");

    let start = document.createElement("div");
    start.className="overlay-text visible";
    start.innerHTML="Tap to Start";
    document.getElementsByTagName("body")[0].appendChild(start);

    let victory=document.createElement("div");
    victory.className="overlay-text";
    victory.id="victory-text";
    victory.innerHTML="Victory";
    let restart=document.createElement("span");
    restart.className="overlay-text-small";
    restart.innerHTML="Click to Restart";
    document.getElementsByTagName("body")[0].appendChild(victory);
    document.getElementById("victory-text").appendChild(restart);
    
    //footer
    let foot=document.createElement("footer");
    let url="https://www.instagram.com/_vaibhav_mehta/"
    foot.innerHTML="Developed by: <a href="+url+">Vaibhav Mehta</a>"
    document.getElementsByTagName("body")[0].appendChild(foot);

    //flip and score//
    let gameScoreContainer=document.createElement("div");
    gameScoreContainer.className="game-info-container";
    document.getElementsByClassName('game-container')[0].appendChild(gameScoreContainer);
    let gameInfo=document.createElement("div");
    gameInfo.className="game-info";
    gameInfo.innerHTML="Flips: ";
    let flipId=document.createElement("span");
    flipId.id="flips";
    flipId.innerHTML="0";
   
    document.getElementsByClassName("game-info-container")[0].appendChild(gameInfo);
    document.getElementsByClassName("game-info")[0].appendChild(flipId);
    
    //generating cards
    var card=[];
    var back=[];
    var front=[];
    var back=[];
    var img=[];
    var imgSrc=[];
    function random_num()
    {
        return  "theme_"+Math.floor((Math.random() * 10) + 1);
    }
    let totCard=9;
    for(let i=0;i<18;i+=2)
    {
        card[i]=document.createElement("div");
        document.getElementsByClassName('game-container')[0].appendChild(card[i]).setAttribute("class","card");
        back[i] = document.createElement("div");
        document.getElementsByClassName('card')[i]
        .appendChild(back[i])
        .setAttribute("class","card-back card-face "+random_num());

        front[i]=document.createElement("div");
        document.getElementsByClassName('card')[i].appendChild(front[i]).setAttribute("class","card-front card-face");
        img[i]=document.createElement("img");
        imgSrc[i]=makeid(5);
        img[i].className="card-value"
        img[i].setAttribute("class","card-value");
        document.getElementsByClassName("card-front")[i].appendChild(img[i]);

        card[i+1] = document.createElement("div");
        document.getElementsByClassName('game-container')[0].appendChild(card[i+1]).setAttribute("class","card");
        back[i+1] = document.createElement("div");
        document.getElementsByClassName('card')[i+1]
        .appendChild(back[i+1])
        .setAttribute("class","card-back card-face "+random_num());

        front[i+1]=document.createElement("div");
        document.getElementsByClassName('card')[i+1]
        .appendChild(front[i+1])
        .setAttribute("class","card-front card-face");

        img[i+1]=document.createElement("img");
        img[i+1].src="https://robohash.org/"+imgSrc[i];
        img[i+1].setAttribute("class","card-value");
        document.getElementsByClassName("card-front")[i+1].appendChild(img[i+1]);

    }

    // cards=document.getElementsByClassName("card");
    // console.log("cards");

    class AudioController{
        constructor(){
            this.bgMusic = new Audio('/public/music/guitar.mp3');
            this.bgMusic.volume = 0.5;
            this.bgMusic.loop = true;
            
        }
        startMusic() {
            this.bgMusic.play();
        }
        stopMusic() {
            this.bgMusic.pause();
            this.bgMusic.currentTime = 0;
        }
    }
    class MixOrMatch {

        constructor(cards) {
            this.cardsArray = cards;
        this.ticker = document.getElementById('flips');
            this.audioController=new AudioController();
            
        }
        startGame() {
            console.log("start");
            console.log(this);
            this.cardToCheck = null;
            this.totalClicks = 0;
            this.matchedCards = [];
            this.busy = true;
            
            setTimeout(() => {
                
                this.shuffleCards();
                this.busy = false;
            }, 600);
            this.hideCards();
            this.ticker.innerText = this.totalClicks;
        }
        hideCards() {
            
            this.cardsArray.forEach(card => {
                card.classList.remove('visible');
                card.classList.remove('matched');
            });
        }
        flipCard(card) {
            if(this.canFlipCard(card)) {
                this.audioController.startMusic();
                this.totalClicks++;
                this.ticker.innerText = this.totalClicks;
                card.classList.add('visible');

                if(this.cardToCheck)
                    this.checkForCardMatch(card);
                else
                    this.cardToCheck = card;
            }
        }
        checkForCardMatch(card) {
            if(this.getCardType(card) === this.getCardType(this.cardToCheck)){
                
                this.cardMatch(card, this.cardToCheck);
            }
                else 
                this.cardMisMatch(card, this.cardToCheck);

            this.cardToCheck = null;
        }
        cardMatch(card1, card2) {
            this.matchedCards.push(card1);
            this.matchedCards.push(card2);
            card1.classList.add('matched');
            card2.classList.add('matched');
            if(this.matchedCards.length === this.cardsArray.length){
                setTimeout(() => {
                    this.shuffleCards();
                    this.busy = false;
                }, 500);
                this.audioController.stopMusic();
                this.victory(); 
            }
        }
        cardMisMatch(card1, card2) {
            this.busy = true;
            setTimeout(() => {
                card1.classList.remove('visible');
                card2.classList.remove('visible');
                this.busy = false;
            }, 1000);
        }
        getCardType(card) {
            return card.getElementsByClassName('card-value')[0].src;
        }
        victory() {
            
            document.getElementById('victory-text').classList.add('visible');
        this.startGame();
        }
        shuffleCards() {
            for(let i = this.cardsArray.length - 1; i > 0; i--) {
                let randIndex = Math.floor(Math.random() * (i+1));
                this.cardsArray[randIndex].style.order = i;
                this.cardsArray[i].style.order = randIndex;
            }   
        }
        canFlipCard(card) {
            return !this.busy && !this.matchedCards.includes(card) && card !== this.cardToCheck;
        }
    }
    function ready() {  
        var cards = Array.from(document.getElementsByClassName('card'));
        let overlays = Array.from(document.getElementsByClassName('overlay-text'));
        
        console.log("ready function");
        let game = new MixOrMatch(cards);
        game.startGame();
        overlays.forEach(overlay => {
            overlay.addEventListener('click', () => {
                overlay.classList.remove('visible');
            });
        });
        cards.forEach(card => {
            function cardClick(){
                    game.flipCard(card);
            }
            card.addEventListener('click', cardClick);
        });
    }
    if(document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', ready);
    } 
    else {
        ready();
    }

};
